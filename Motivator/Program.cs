﻿using System;
using System.IO;

namespace Motivator
{
    class Program
    {
        static void Main(string[] args)
        {
            var path  = Path.GetFullPath("Images");
            Console.Write("Введите номер картинки от 1 до 5: ");
            var fileImage = $"{path}/{Console.ReadLine()}.jpg";           
            Console.Write("Введите текст мотиватора: ");
            var textMotivator = Console.ReadLine();
            Console.WriteLine(Motivator.CreateMotivator(fileImage, textMotivator));
            Console.ReadLine();
        }
    }
}