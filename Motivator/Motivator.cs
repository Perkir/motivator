using System.Drawing;
using Motivator.MotivatorActions;

namespace Motivator
{
    public class Motivator
    {
        public static string CreateMotivator(string image, string message)
        {
            
            var backGround = new BackGround(178, 290);
            var proxy = new Proxy(backGround);
            proxy.SetBackground(backGround.Width, backGround.Height, backGround.Image);
            
            var frame = new Frame(300, backGround);
            var loadPicture = new Picture(Image.FromFile(image), frame);
            var pictureName = new PictureName(message, loadPicture);
            pictureName.Execute();
            
            var motivator = pictureName.Image;
            motivator.Save(motivator.ToString());

            return $"Мотиватор {motivator} сохранен";
        }
    }
}