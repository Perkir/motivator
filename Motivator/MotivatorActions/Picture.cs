using System.Drawing;

namespace Motivator.MotivatorActions
{
    public class Picture: Base
    {
        private new Image Image { get; set; }

        public override void Execute()
        {
            Graphic.DrawImage(Image, Width / 2 - Image.Width/2, Height / 2 - Image.Height/2, Image.Width, Image.Height);
        }

        public Picture(Image picture, IComponent component): base(component)
        {
            Image = picture;
        }
    }
}