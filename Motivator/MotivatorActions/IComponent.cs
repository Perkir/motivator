using System.Drawing;

namespace Motivator.MotivatorActions
{
    public interface IComponent
    {
        Graphics Graphic { get; set; }
        Image Image { get; set; }
        int Width { get; set; }
        int Height { get; set; }
        void Execute();

    }
}