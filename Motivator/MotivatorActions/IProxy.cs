using System.Drawing;

namespace Motivator.MotivatorActions
{
    public interface IProxy
    {
        void SetBackground(int width, int height, Image image);
    }
}