using System.Drawing;

namespace Motivator.MotivatorActions
{
    public class Frame: Base
    {
        private new int Width{ get; set; }

        public Frame(int width, IComponent component): base(component)
        {
            Width = width;
        }

        public override void Execute()
        {
            base.Execute();
            Point[] points =  { 
                new Point(Width / 2 - 50, Height / 2 - 50), 
                new Point(Width / 2 + 50, Height / 2 - 50), 
                new Point(Width / 2 + 50, Height / 2 + 50), 
                new Point(Width / 2 - 50, Height / 2 + 50), 
                new Point(Width / 2 - 50, Height / 2 - 60) 
            };            
            Graphic.DrawLines(new Pen(Color.White, Width),points);            
        }

    }
}