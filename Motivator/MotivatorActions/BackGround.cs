using System.Drawing;

namespace Motivator.MotivatorActions
{
    public class BackGround: IComponent,IProxy
    {
        public Graphics Graphic { get; set;}
        public Image Image { get; set;}
        public int Width { get; set;}
        public int Height {get;set;}
        public void Execute() { }

        public BackGround(int width, int height)
        {
            Width = width;
            Height = height;
            Image = new Bitmap(Width, Height);
            Graphic = Graphics.FromImage(Image);
        }

        public void SetBackgroundWithProxy(Proxy proxy, int width, int height, Image image)
        {
            proxy.SetBackground(width, height, image);
        }

        public void SetBackground(int width, int height, Image image)
        {
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    ((Bitmap)image).SetPixel(i, j, Color.Black);
                }
            }
            
        }
    }
}