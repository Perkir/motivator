using System.Drawing;

namespace Motivator.MotivatorActions
{
    public class Base: IComponent
    {
        private readonly IComponent _nextComponent;
        public Graphics Graphic { get; set; }

        public Image Image { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        protected Base(IComponent nextComponent)
        {
            this._nextComponent = nextComponent;
            Width = nextComponent.Width;
            Height = nextComponent.Height;
            Graphic = nextComponent.Graphic;
            Image = nextComponent.Image;
        }
        public virtual void Execute()
        {          
            _nextComponent.Execute();
        }

    }
}