using System;
using System.Drawing;

namespace Motivator.MotivatorActions
{
    public class Proxy: IProxy
    {
        
        private BackGround _backGround;
        
        public Proxy(BackGround backGround)
        {
            _backGround = backGround;
        }
        
        public void SetBackground(int width, int height, Image image)
        {
            _backGround = new BackGround(300, 300);
            Console.WriteLine("Прокси поменял размер картинки на 300 на 300");
            _backGround.SetBackground(width, height,image);
        }
    }
}