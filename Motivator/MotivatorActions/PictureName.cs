using System.Drawing;

namespace Motivator.MotivatorActions
{
    public class PictureName: Base
    {
        private readonly string _pictureName;

        public PictureName(string text, IComponent component): base(component)
        {
            _pictureName = text;
        }

        public override void Execute()
        {
            base.Execute();
            Graphic.DrawString(_pictureName, new Font("Arial", 20),
                new SolidBrush(Color.White), Width/2 - _pictureName.Length*5, Height - 60);
        }
    }
}